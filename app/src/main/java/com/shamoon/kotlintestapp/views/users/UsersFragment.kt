package com.shamoon.kotlintestapp.views.users

import android.content.Context
import android.os.Bundle
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.shamoon.kotlintestapp.R
import com.shamoon.kotlintestapp.databinding.UsersFragmentBinding
import com.shamoon.kotlintestapp.model.api.Item

class UsersFragment : Fragment() {

    companion object {
        fun newInstance() = UsersFragment()
    }

    private lateinit var viewModel: UsersViewModel
    private lateinit var binding: UsersFragmentBinding
    private lateinit var mListener: UsersListener

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.users_fragment, container, false)
        activity!!.title = "Users"
        return binding.getRoot()
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setupBindings(savedInstanceState)
    }

    private fun setupBindings(savedInstanceState: Bundle?) {
        viewModel = ViewModelProvider(this).get(UsersViewModel::class.java)

        if (savedInstanceState == null) {
            viewModel.init();
        }
        binding.setModel(viewModel);

        setupListUpdate();
    }

    private fun setupListUpdate() {
        viewModel.loading.set(View.VISIBLE)
        binding.etSearch.setText("")
        binding.btnSearch.setOnClickListener { view ->
            run {
                if (!TextUtils.equals(binding.etSearch.text.toString(), "")){
                    viewModel.fetchList(
                        1,
                        resources.getInteger(R.integer.page_size),
                        getString(R.string.asc),
                        getString(R.string.name),
                        getString(R.string.stackoverflow),
                        binding.etSearch.text.toString()
                    )
                }
            }
        }
        viewModel.fetchList(
            1,
            resources.getInteger(R.integer.page_size),
            getString(R.string.asc),
            getString(R.string.name),
            getString(R.string.stackoverflow)
        )

        val observer = Observer<ArrayList<Item>> { users ->
            // Update the UI, in this case, a TextView.
            viewModel.loading.set(View.GONE)
            if (users.size == 0) {
                viewModel.showEmpty.set(View.VISIBLE)
            } else {
                viewModel.showEmpty.set(View.GONE)
                viewModel.setUsersInAdapter(users)
            }
        }

        viewModel.getUsers().observe(viewLifecycleOwner, observer)

        setupListClick()
    }

    private fun setupListClick() {
        val observer = Observer<Item> { user ->
            if (user != null) {
                viewModel.onStop()
                mListener.onUserClick(user)
            }
        }
        viewModel.selected.observe(viewLifecycleOwner, observer)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)

        if (context is UsersListener) {
            mListener = context as UsersListener
        }
    }


    interface UsersListener {
        fun onUserClick(user: Item)
    }

}
