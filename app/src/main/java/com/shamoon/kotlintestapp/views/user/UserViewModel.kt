package com.shamoon.kotlintestapp.views.user

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import androidx.lifecycle.ViewModel
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.shamoon.kotlintestapp.model.api.Item


class UserViewModel : ViewModel() {
    private lateinit var user: Item

    fun setUser(user: Item){
        this.user = user
    }

    fun getUser(): Item{
        return user
    }

    fun getUserName(): String{
        return "User Name: " + user.displayName
    }

    fun getReputation(): String{
        return "Reputation: " + user.reputation.toShort()
    }

    fun getBadges(): String{
        return "Badges: " + user.badgeCounts.toString()
    }

    fun getLocation(): String{
        return "Location: " + user.location
    }

    fun getAge(): String{
        return "Age: " + user.creationDate.toString()
    }

    fun getCreationDate(): String{
        return "Creation Date: " + user.creationDate.toString()
    }

    companion object {
        @JvmStatic
        @BindingAdapter("profileImage")
        fun loadImage(view: ImageView, imageUrl: String?) {
            Glide.with(view.getContext())
                .load(imageUrl)
                .apply(RequestOptions())
                .into(view)
        }
    }

}
