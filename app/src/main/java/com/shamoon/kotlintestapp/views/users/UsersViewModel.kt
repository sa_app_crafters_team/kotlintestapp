package com.shamoon.kotlintestapp.views.users

import android.view.View
import androidx.databinding.ObservableInt
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.shamoon.kotlintestapp.R
import com.shamoon.kotlintestapp.controller.adapters.UsersAdapter
import com.shamoon.kotlintestapp.model.api.Item
import com.shamoon.kotlintestapp.model.repositories.UsersRepository

class UsersViewModel : ViewModel() {
    lateinit var loading: ObservableInt
    lateinit var showEmpty: ObservableInt
    private lateinit var usersRepository: UsersRepository
    private lateinit var adapter: UsersAdapter

    lateinit var selected: MutableLiveData<Item>

    fun init() {
        usersRepository = UsersRepository()
        adapter = UsersAdapter(R.layout.user_list_item, this)
        loading = ObservableInt(View.GONE)
        showEmpty = ObservableInt(View.GONE)
        selected = MutableLiveData()
    }

    fun onStop() {
        usersRepository.clearRepository()
    }

    fun fetchList(page: Int, pageSize: Int, order: String, sort: String, site: String) {
        usersRepository.fetchList(page, pageSize, order, sort, site)
    }

    fun fetchList(page: Int, pageSize: Int, order: String, sort: String, site: String, name: String) {
        usersRepository.fetchList(page, pageSize, order, sort, site, name)
    }

    fun getUsers(): MutableLiveData<ArrayList<Item>> {
        return usersRepository.users
    }

    fun getAdapter(): UsersAdapter {
        return adapter
    }

    fun setUsersInAdapter(categories: ArrayList<Item>) {
        adapter.setUsers(categories)
        adapter.notifyDataSetChanged()
    }

    fun onItemClick(index: Int?) {
        val db: Item? = getUserAt(index)
        selected.setValue(db)
    }

    fun getUserAt(index: Int?): Item? {
        return if (usersRepository.users
                .getValue() != null && index != null && usersRepository.users
                .getValue()!!.size > index
        ) {
            usersRepository.users.getValue()!!.get(index)
        } else null
    }

}
