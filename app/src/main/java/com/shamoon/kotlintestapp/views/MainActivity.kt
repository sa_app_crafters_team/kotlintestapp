package com.shamoon.kotlintestapp.views

import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.FragmentManager
import com.shamoon.kotlintestapp.R
import com.shamoon.kotlintestapp.model.api.Item
import com.shamoon.kotlintestapp.views.user.UserFragment
import com.shamoon.kotlintestapp.views.users.UsersFragment

class MainActivity : AppCompatActivity(), UsersFragment.UsersListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                .replace(R.id.container, UsersFragment.newInstance(), "UsersFragment")
                .commit()
        }
        supportFragmentManager.addOnBackStackChangedListener { setupHomeAsUp() }
        setupHomeAsUp()
    }

    private fun setupHomeAsUp() {
        val shouldShow = 0 < supportFragmentManager.backStackEntryCount
        supportActionBar?.setDisplayHomeAsUpEnabled(shouldShow)
    }

    override fun onUserClick(user: Item) {
        supportFragmentManager.beginTransaction()
            .replace(R.id.container, UserFragment.newInstance(user))
            .addToBackStack("UserFragment")
            .commit()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.getItemId()) {
            android.R.id.home -> {
                val fm: FragmentManager = supportFragmentManager
                if (fm.getBackStackEntryCount() > 0) {
                    fm.popBackStack()
                    return true
                }
            }
        }
        return super.onOptionsItemSelected(item)
    }
}
