package com.shamoon.kotlintestapp.views.user

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelStoreOwner
import com.shamoon.kotlintestapp.R
import com.shamoon.kotlintestapp.databinding.UserFragmentBinding
import com.shamoon.kotlintestapp.model.api.Item

class UserFragment : Fragment(), ViewModelStoreOwner {

    companion object {
        fun newInstance() = UserFragment()

        @JvmStatic
        fun newInstance(user: Item) = UserFragment().apply {
            arguments = Bundle().apply {
                putSerializable("user", user)
            }
        }
    }

    private lateinit var viewModel: UserViewModel
    private lateinit var binding: UserFragmentBinding
    private lateinit var user: Item


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.user_fragment, container, false)
        activity!!.title = user.displayName.toString()

        return binding.getRoot()
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setupBindings(savedInstanceState)
    }

    private fun setupBindings(savedInstanceState: Bundle?) {
        viewModel = ViewModelProvider(this).get(UserViewModel::class.java)
        viewModel.setUser(user)
        binding.userModel = viewModel
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        arguments?.getSerializable("user")?.let {
            user = it as Item
        }

    }

}
