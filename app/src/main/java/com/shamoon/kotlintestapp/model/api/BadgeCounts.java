
package com.shamoon.kotlintestapp.model.api;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "bronze",
    "silver",
    "gold"
})
public class BadgeCounts {

    @JsonProperty("bronze")
    private Integer bronze;
    @JsonProperty("silver")
    private Integer silver;
    @JsonProperty("gold")
    private Integer gold;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("bronze")
    public Integer getBronze() {
        return bronze;
    }

    @JsonProperty("bronze")
    public void setBronze(Integer bronze) {
        this.bronze = bronze;
    }

    @JsonProperty("silver")
    public Integer getSilver() {
        return silver;
    }

    @JsonProperty("silver")
    public void setSilver(Integer silver) {
        this.silver = silver;
    }

    @JsonProperty("gold")
    public Integer getGold() {
        return gold;
    }

    @JsonProperty("gold")
    public void setGold(Integer gold) {
        this.gold = gold;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
