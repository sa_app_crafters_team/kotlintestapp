
package com.shamoon.kotlintestapp.model.api;

import androidx.databinding.BaseObservable;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "badge_counts",
    "account_id",
    "is_employee",
    "last_modified_date",
    "last_access_date",
    "reputation_change_year",
    "reputation_change_quarter",
    "reputation_change_month",
    "reputation_change_week",
    "reputation_change_day",
    "reputation",
    "creation_date",
    "user_type",
    "user_id",
    "accept_rate",
    "location",
    "website_url",
    "link",
    "profile_image",
    "display_name"
})
public class Item extends BaseObservable implements Serializable {

    @JsonProperty("badge_counts")
    private BadgeCounts badgeCounts;
    @JsonProperty("account_id")
    private Integer accountId;
    @JsonProperty("is_employee")
    private Boolean isEmployee;
    @JsonProperty("last_modified_date")
    private Integer lastModifiedDate;
    @JsonProperty("last_access_date")
    private Integer lastAccessDate;
    @JsonProperty("reputation_change_year")
    private Integer reputationChangeYear;
    @JsonProperty("reputation_change_quarter")
    private Integer reputationChangeQuarter;
    @JsonProperty("reputation_change_month")
    private Integer reputationChangeMonth;
    @JsonProperty("reputation_change_week")
    private Integer reputationChangeWeek;
    @JsonProperty("reputation_change_day")
    private Integer reputationChangeDay;
    @JsonProperty("reputation")
    private Integer reputation;
    @JsonProperty("creation_date")
    private Integer creationDate;
    @JsonProperty("user_type")
    private String userType;
    @JsonProperty("user_id")
    private Integer userId;
    @JsonProperty("accept_rate")
    private Integer acceptRate;
    @JsonProperty("location")
    private String location;
    @JsonProperty("website_url")
    private String websiteUrl;
    @JsonProperty("link")
    private String link;
    @JsonProperty("profile_image")
    private String profileImage;
    @JsonProperty("display_name")
    private String displayName;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("badge_counts")
    public BadgeCounts getBadgeCounts() {
        return badgeCounts;
    }

    @JsonProperty("badge_counts")
    public void setBadgeCounts(BadgeCounts badgeCounts) {
        this.badgeCounts = badgeCounts;
    }

    @JsonProperty("account_id")
    public Integer getAccountId() {
        return accountId;
    }

    @JsonProperty("account_id")
    public void setAccountId(Integer accountId) {
        this.accountId = accountId;
    }

    @JsonProperty("is_employee")
    public Boolean getIsEmployee() {
        return isEmployee;
    }

    @JsonProperty("is_employee")
    public void setIsEmployee(Boolean isEmployee) {
        this.isEmployee = isEmployee;
    }

    @JsonProperty("last_modified_date")
    public Integer getLastModifiedDate() {
        return lastModifiedDate;
    }

    @JsonProperty("last_modified_date")
    public void setLastModifiedDate(Integer lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    @JsonProperty("last_access_date")
    public Integer getLastAccessDate() {
        return lastAccessDate;
    }

    @JsonProperty("last_access_date")
    public void setLastAccessDate(Integer lastAccessDate) {
        this.lastAccessDate = lastAccessDate;
    }

    @JsonProperty("reputation_change_year")
    public Integer getReputationChangeYear() {
        return reputationChangeYear;
    }

    @JsonProperty("reputation_change_year")
    public void setReputationChangeYear(Integer reputationChangeYear) {
        this.reputationChangeYear = reputationChangeYear;
    }

    @JsonProperty("reputation_change_quarter")
    public Integer getReputationChangeQuarter() {
        return reputationChangeQuarter;
    }

    @JsonProperty("reputation_change_quarter")
    public void setReputationChangeQuarter(Integer reputationChangeQuarter) {
        this.reputationChangeQuarter = reputationChangeQuarter;
    }

    @JsonProperty("reputation_change_month")
    public Integer getReputationChangeMonth() {
        return reputationChangeMonth;
    }

    @JsonProperty("reputation_change_month")
    public void setReputationChangeMonth(Integer reputationChangeMonth) {
        this.reputationChangeMonth = reputationChangeMonth;
    }

    @JsonProperty("reputation_change_week")
    public Integer getReputationChangeWeek() {
        return reputationChangeWeek;
    }

    @JsonProperty("reputation_change_week")
    public void setReputationChangeWeek(Integer reputationChangeWeek) {
        this.reputationChangeWeek = reputationChangeWeek;
    }

    @JsonProperty("reputation_change_day")
    public Integer getReputationChangeDay() {
        return reputationChangeDay;
    }

    @JsonProperty("reputation_change_day")
    public void setReputationChangeDay(Integer reputationChangeDay) {
        this.reputationChangeDay = reputationChangeDay;
    }

    @JsonProperty("reputation")
    public Integer getReputation() {
        return reputation;
    }

    @JsonProperty("reputation")
    public void setReputation(Integer reputation) {
        this.reputation = reputation;
    }

    @JsonProperty("creation_date")
    public Integer getCreationDate() {
        return creationDate;
    }

    @JsonProperty("creation_date")
    public void setCreationDate(Integer creationDate) {
        this.creationDate = creationDate;
    }

    @JsonProperty("user_type")
    public String getUserType() {
        return userType;
    }

    @JsonProperty("user_type")
    public void setUserType(String userType) {
        this.userType = userType;
    }

    @JsonProperty("user_id")
    public Integer getUserId() {
        return userId;
    }

    @JsonProperty("user_id")
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    @JsonProperty("accept_rate")
    public Integer getAcceptRate() {
        return acceptRate;
    }

    @JsonProperty("accept_rate")
    public void setAcceptRate(Integer acceptRate) {
        this.acceptRate = acceptRate;
    }

    @JsonProperty("location")
    public String getLocation() {
        return location;
    }

    @JsonProperty("location")
    public void setLocation(String location) {
        this.location = location;
    }

    @JsonProperty("website_url")
    public String getWebsiteUrl() {
        return websiteUrl;
    }

    @JsonProperty("website_url")
    public void setWebsiteUrl(String websiteUrl) {
        this.websiteUrl = websiteUrl;
    }

    @JsonProperty("link")
    public String getLink() {
        return link;
    }

    @JsonProperty("link")
    public void setLink(String link) {
        this.link = link;
    }

    @JsonProperty("profile_image")
    public String getProfileImage() {
        return profileImage;
    }

    @JsonProperty("profile_image")
    public void setProfileImage(String profileImage) {
        this.profileImage = profileImage;
    }

    @JsonProperty("display_name")
    public String getDisplayName() {
        return displayName;
    }

    @JsonProperty("display_name")
    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
