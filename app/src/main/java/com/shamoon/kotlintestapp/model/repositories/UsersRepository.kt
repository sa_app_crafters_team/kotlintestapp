package com.shamoon.kotlintestapp.model.repositories

import androidx.lifecycle.MutableLiveData
import com.shamoon.kotlintestapp.controller.service.Api
import com.shamoon.kotlintestapp.model.api.Item
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.schedulers.Schedulers
import io.reactivex.rxjava3.subjects.PublishSubject


class UsersRepository {
    private val TAG: String = UsersRepository::class.java.getSimpleName()
    private val userList: ArrayList<Item> = ArrayList()
    private val disposable = CompositeDisposable()
    private val publishSubject = PublishSubject.create<String>()
    val users: MutableLiveData<ArrayList<Item>> by lazy {
        MutableLiveData<ArrayList<Item>>()
    }


    fun fetchList(page: Int, pageSize: Int, order: String, sort: String, site: String) {

        disposable.add(Api().getApi()!!.getUsersSingle(page, pageSize, order, sort, site)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { response -> users.setValue(response!!.items as ArrayList<Item>?) })
    }

    fun fetchList(page: Int, pageSize: Int, order: String, sort: String, site: String, name: String) {

        disposable.add(Api().getApi()!!.getUsersSingle(page, pageSize, order, sort, site, name)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { response -> users.setValue(response!!.items as ArrayList<Item>?) })
    }

    fun clearRepository() {
        disposable.clear()
    }

}