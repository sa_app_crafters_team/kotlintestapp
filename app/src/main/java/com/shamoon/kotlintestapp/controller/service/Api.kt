package com.shamoon.kotlintestapp.controller.service

import retrofit2.Retrofit
import retrofit2.adapter.rxjava3.RxJava3CallAdapterFactory
import retrofit2.converter.jackson.JacksonConverterFactory


class Api {
    private var api: ApiService? = null
    private val BASE_URL = "https://api.stackexchange.com/2.2/"

    public fun getApi(): ApiService? {
        if (api == null) {
            val retrofit = Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(JacksonConverterFactory.create())
                .addCallAdapterFactory(RxJava3CallAdapterFactory.create())
                .build()
            api = retrofit.create(ApiService::class.java)
        }


        return api
    }
}