package com.shamoon.kotlintestapp.controller.adapters;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;

import com.shamoon.kotlintestapp.BR;
import com.shamoon.kotlintestapp.databinding.UserListItemBinding;
import com.shamoon.kotlintestapp.model.api.Item;
import com.shamoon.kotlintestapp.views.users.UsersViewModel;

import java.util.ArrayList;

public class UsersAdapter extends RecyclerView.Adapter<UsersAdapter.GenericViewHolder> {

    private int layoutId;
    private ArrayList<Item> users;
    private UsersViewModel viewModel;

    public UsersAdapter(@LayoutRes int layoutId, UsersViewModel viewModel) {
        users = new ArrayList<>();
        this.layoutId = layoutId;
        this.viewModel = viewModel;
    }

    private int getLayoutIdForPosition(int position) {
        return layoutId;
    }

    @NonNull
    @Override
    public GenericViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        UserListItemBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), layoutId, parent, false);

        return new GenericViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull GenericViewHolder holder, int position) {
        holder.bind(viewModel, position);
    }

    @Override
    public int getItemViewType(int position) {
        return getLayoutIdForPosition(position);
    }

    public void setUsers(ArrayList<Item> users) {
        this.users.clear();
        this.users.addAll(users);
    }

    @Override
    public int getItemCount() {
        if (users == null)
            return 0;
        return users.size();
    }

    class GenericViewHolder extends RecyclerView.ViewHolder {
        ViewDataBinding binding;

        public GenericViewHolder(@NonNull ViewDataBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        void bind(UsersViewModel viewModel, int position) {
            binding.setVariable(BR.position, position);
            binding.setVariable(BR.userViewModel    , viewModel);
            binding.executePendingBindings();
        }

    }

}
