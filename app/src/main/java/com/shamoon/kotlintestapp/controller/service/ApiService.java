package com.shamoon.kotlintestapp.controller.service;

import com.shamoon.kotlintestapp.model.api.UsersResponse;

import io.reactivex.rxjava3.core.Single;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ApiService {

    @GET("users")
    Single<UsersResponse> getUsersSingle(@Query("page") int page, @Query("pagesize") int pageSize, @Query("order") String order, @Query("sort") String sort, @Query("site") String site);
    @GET("users")
    Single<UsersResponse> getUsersSingle(@Query("page") int page, @Query("pagesize") int pageSize, @Query("order") String order, @Query("sort") String sort, @Query("site") String site, @Query("inname") String inname);
}
